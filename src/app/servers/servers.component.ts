import { Component, OnInit } from '@angular/core';

@Component({
      selector : 'app-servers',
  // template: `<app-server></app-server><app-server></app-server>`,
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowedNewServer = false;
  serverCreationStatus = 'No server was created';
serverName = "Testserver";
serverCreated = false;
servers = ['Test-Server', 'Test-Server2']



  constructor() {
    setTimeout(() =>{
  this.allowedNewServer = true;
    }, 2000);
  }

  ngOnInit(): void {
  }

  onCreateServer () {
    this.serverCreated = true;
    this.servers.push(this.serverName);
    this.serverCreationStatus = 'Server was created ' + this.serverName;

  }
  onOpdateServerName (event : any) {
    this.serverName = (<HTMLInputElement>event.target).value;
  }
}
